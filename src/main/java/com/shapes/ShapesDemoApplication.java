package com.shapes;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.MessageSource;

import com.shapes.model.Shape2DEnum;
import com.shapes.service.ShapeService;
import com.shapes.service.TestProperties;


@EnableConfigurationProperties
@SpringBootApplication
public class ShapesDemoApplication implements CommandLineRunner{

	
	@Value("${com.any.test.p1}")
	private String val1;
	@Value("${com.any.test.p2}")
	private Double val2;
	
	@Autowired
	private MessageSource messageSource;
	
	
	private final Logger logger =  
		LoggerFactory.getLogger(ShapesDemoApplication.class);
	
	private final ShapeService shapeService;
	
	@Autowired
	private TestProperties testProperties;
	
	public ShapesDemoApplication(ShapeService shapeService) {
		super();
		this.shapeService = shapeService;
	}

	public static void main(String[] args) {
		SpringApplication.run(ShapesDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
//		logger.info("Retriangle perim {}",shapeService
//				.calculatePerimeter2D(Shape2DEnum.TRIANGLE.getValue(), 5.0,5.0,5.0));
//		logger.info("Retriangle area {}",shapeService
//				.calculateArea2D(Shape2DEnum.TRIANGLE.getValue(), 5.0,5.0));
//		
//		logger.info("Retriangle perim {}",shapeService
//				.calculatePerimeter2D(Shape2DEnum.RETRANGLE.getValue(), 5.0,5.0));
//		logger.info("Retriangle area {}",shapeService
//				.calculateArea2D(Shape2DEnum.RETRANGLE.getValue(), 5.0,5.0));
//		
	
		logger.info("{}",testProperties);
		
		logger.info("v1={}, v2={}",val1,val2);
		
		logger.info("message en : {}",messageSource.getMessage("exceptions.msg1", null,Locale.ENGLISH));
		logger.info("message en : {}",messageSource.getMessage("exceptions.msg1", null,Locale.FRENCH));

	}

}
