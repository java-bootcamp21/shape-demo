package com.shapes.service;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

@Service
@ConfigurationProperties("com.any.test")
public class TestProperties {
	
	private String p1;
	private Double p2;
	public String getP1() {
		return p1;
	}
	public void setP1(String p1) {
		this.p1 = p1;
	}
	public Double getP2() {
		return p2;
	}
	public void setP2(Double p2) {
		this.p2 = p2;
	}
	@Override
	public String toString() {
		return "TestProperties [p1=" + p1 + ", p2=" + p2 + "]";
	}
	
	
	
	
	

}
