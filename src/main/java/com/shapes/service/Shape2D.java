package com.shapes.service;

public interface Shape2D extends Shape{

	Double calcPerimeter(Double...vals);
	Double calcArea(Double...vals);
}
