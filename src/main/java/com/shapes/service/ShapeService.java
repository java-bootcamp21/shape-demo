package com.shapes.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class ShapeService {

//	private final List<Shape2D> shape2ds;

//	public ShapeService(List<Shape2D> shape2ds) {
//		this.shape2ds = shape2ds;
//	}

	private final Shape2D retriangleServiceShape2d;
	private final Shape2D trekendeshi;
	public ShapeService(Shape2D retriangleServiceShape2d, @Qualifier("trekendesh") Shape2D trekendeshi) {
		super();
		this.retriangleServiceShape2d = retriangleServiceShape2d;
		this.trekendeshi = trekendeshi;
	}

	

//	public Double calculateArea2D(String shapeType, Double... vals) {
//		return shape2ds.stream()
//				.filter(s -> s.isSupported(shapeType))
//				.findFirst()
//				.map(s-> s.calcArea(vals))
//				.orElseThrow(()-> new RuntimeException("service not found "));
//				
//	}
//	
//	public Double calculatePerimeter2D(String shapeType, Double... vals  ) {
//		return shape2ds.stream()
//				.filter(s -> s.isSupported(shapeType))
//				.findFirst()
//				.map(s-> s.calcPerimeter(vals))
//				.orElseThrow(()-> new RuntimeException("service not found "));
//	}

}
