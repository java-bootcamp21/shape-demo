package com.shapes.service.impl;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.shapes.model.Shape2DEnum;
import com.shapes.service.Shape2D;

@Primary
@Service
public class RectriangleService implements Shape2D{

	private final Shape2DEnum shape = Shape2DEnum.RETRANGLE;

	@Override
	public Boolean isSupported(String val) {
		return shape == Shape2DEnum.fromValue(val);
	}

	@Override
	public Double calcPerimeter(Double... vals) {
		return (2*vals[0])+(2*vals[1]);
	}

	@Override
	public Double calcArea(Double... vals) {
		return (vals[0]*vals[1])/3;
	}
	
}
