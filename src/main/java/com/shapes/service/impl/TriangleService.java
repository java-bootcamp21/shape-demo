package com.shapes.service.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.shapes.model.Shape2DEnum;
import com.shapes.service.Shape2D;

@Qualifier("trekendesh")
@Service
public class TriangleService implements Shape2D{

	private final Shape2DEnum shape = Shape2DEnum.TRIANGLE;

	@Override
	public Boolean isSupported(String val) {
		return shape == Shape2DEnum.fromValue(val);
	}

	@Override
	public Double calcPerimeter(Double... vals) {
		return vals[0]+vals[1]+vals[2];
	}

	@Override
	public Double calcArea(Double... vals) {
		return (vals[0]*vals[1])/2;
	}
	
}
