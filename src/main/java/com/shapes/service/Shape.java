package com.shapes.service;

public interface Shape {
	Boolean isSupported(String shape);
}
