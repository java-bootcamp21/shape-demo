package com.shapes.model;

import java.util.Arrays;

public enum Shape2DEnum {
	
	RETRANGLE("drejtkendesh"),
	TRIANGLE("trekendesh");
	
	private String value;

	private Shape2DEnum(String value) {
		this.value = value;
	}
	
	public static Shape2DEnum fromValue(String value) {
		return Arrays.asList(Shape2DEnum.values())
				.stream()
				.filter(e -> e.getValue().equals(value))
				.findFirst()
				.orElseThrow(()-> new RuntimeException("enum not found"));
							
	}

	public String getValue() {
		return value;
	}	
	
}
