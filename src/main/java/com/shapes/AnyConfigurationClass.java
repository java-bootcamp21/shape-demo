package com.shapes;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import com.shapes.service.ConditionalOnPropertyClass;

@Configuration
public class AnyConfigurationClass {
	
	@ConditionalOnProperty(name = "com.enable",havingValue = "true")
	@Bean
	public ConditionalOnPropertyClass init() {
		return new ConditionalOnPropertyClass();
	}
	
	@Bean
	public MessageSource initMessageSource() {
		var source = new ReloadableResourceBundleMessageSource();
		source.setBasename("classpath:messages");
		source.setDefaultEncoding("UTF-8");
		return source;
	}

}
